using System;
using NUnit.Framework;
using NBWeather.UI.Android;

namespace NBWeather.Test
{
	[TestFixture]
	public class MeasurementInfoTest
	{
		[Test]
		public void Parse1()
		{
			const string sample = "\n        <p>\n        Temp: -0.6 C<br>\n        Wind Chill: -2.9 C<br>\n        Heat Index: -0.6 C<br>\n";
			
			MeasurementInfo info = MeasurementInfo.Parse(sample);
			Assert.AreEqual(-0.6f, info.Temp);
			Assert.AreEqual(-2.9f, info.WindChill);
			Assert.AreEqual(0, info.Humidity);
		}

		[Test]
		public void Parse2()
		{
			const string sample = "<p> Temp: 0.3 C<br> Wind Chill: -3.3 C<br> Heat Index: 0.3 C<br> Humidity: 84 %<br> Dewpoint: -2.1 C<br> Barometer: 1005.3 mb<br> Wind: SSE at 3.1 m/s<br> Rain Today: 0.52 mm<br> Rain Rate: 0.00 mm<br> </p>";
			
			MeasurementInfo info = MeasurementInfo.Parse(sample);
			Assert.AreEqual(0.3f, info.Temp);
			Assert.AreEqual(-3.3f, info.WindChill);
			Assert.AreEqual(84, info.Humidity);
			Assert.AreEqual(1005, info.Barometer);
		}
		
		[Test]
		public void Parse3()
		{
			const string sample = "<p> Temp: 1.6 C<br> Wind Chill: -0.3 C<br> Heat Index: 1.6 C<br> Humidity: 90 %<br> Dewpoint: 0.1 C<br> Barometer: 1008.8 mb<br> Wind: W at 1.8 m/s<br> Rain Today: 1.03 mm<br> Rain Rate: 0.00 mm<br> </p>";
			
			MeasurementInfo info = MeasurementInfo.Parse(sample);
			Assert.AreEqual(1.6f, info.Temp);
			Assert.AreEqual(-0.3f, info.WindChill);
			Assert.AreEqual(90, info.Humidity);
			Assert.AreEqual(1008, info.Barometer);
		}


	}
}

