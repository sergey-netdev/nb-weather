using System;
using System.Linq;
using System.Net;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Linq;
using System.Threading;
using Android.App;
using Android.Appwidget;
using Android.Content;
using Android.OS;
using Android.Widget;
using Android.Views;
using Android.Util;

namespace NBWeather.UI.Android
{
	[Service]
	public class UpdateService : Service
	{
		const string appName = "NB-W";

		ComponentName thisWidget;
		AppWidgetManager manager;
		RemoteViews updateViews;

		public override void OnStart (Intent intent, int startId)
		{
			updateViews = new RemoteViews (this.PackageName, Resource.Layout.Widget);
			thisWidget = new ComponentName (this, Java.Lang.Class.FromType(typeof(Widget)));
			manager = AppWidgetManager.GetInstance (this);

			// When user clicks on widget rerun service update
			Intent updateIntent = new Intent(this, typeof(UpdateService));
			PendingIntent pendingIntent = PendingIntent.GetService(this, 0, updateIntent, 0);
			updateViews.SetOnClickPendingIntent (Resource.Id.widget, pendingIntent);

			// When user clicks on button open web page
			Intent detailsIntent = new Intent(Intent.ActionView, global::Android.Net.Uri.Parse("http://johnson-baby.info/wviev/"));
			pendingIntent = PendingIntent.GetActivity(this, 0, detailsIntent, PendingIntentFlags.CancelCurrent);
			updateViews.SetOnClickPendingIntent (Resource.Id.buttonDetails, pendingIntent);

			// set background to indicate update
			updateViews.SetInt(Resource.Id.widget, "setBackgroundResource", Resource.Drawable.myshapeerror);
			manager.UpdateAppWidget (thisWidget, updateViews);

			ThreadPool.QueueUserWorkItem(state => {
				Log.Debug(appName, "Starting background worker. Culture: {0}.", Thread.CurrentThread.CurrentCulture.Name);
				Log.Debug(appName, "NumberDecimalSeparator: '{0}'.", Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator);
//				Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
				var svc = (UpdateService)state;
				try
				{
					var info = Update();
					if (info != null)
					{
						svc.updateViews.SetTextViewText (Resource.Id.textViewTemp, string.Format("T: {0}°", info.Temp));
						svc.updateViews.SetTextViewText (Resource.Id.textViewHumidity, string.Format("H: {0}%", info.Humidity));
						svc.updateViews.SetTextViewText (Resource.Id.textViewBarometer, string.Format("B: {0} mb", info.Barometer));
						svc.updateViews.SetTextViewText (Resource.Id.textViewRain, string.Format("R: {0}/{1} mm", info.RainToday, info.RainRate));
						svc.updateViews.SetTextViewText (Resource.Id.textViewWind, string.Format("W: {0} {1} m/s", info.WindDirection, info.WindSpeed));

						svc.updateViews.SetInt(Resource.Id.widget, "setBackgroundResource", Resource.Drawable.myshape);
					}
					svc.manager.UpdateAppWidget (thisWidget, updateViews);
					svc.StopSelf();
				}
				catch (Exception ex)
				{
					Log.Error(appName, ex.ToString());
				}
				finally
				{
					Log.Debug(appName, "Finished.");
				}

			}, this);
		}
		
		public override IBinder OnBind (Intent intent)
		{
			// We don't need to bind to this service
			return null;
		}


		MeasurementInfo Update()
		{
			MeasurementInfo result = null;
			try {
				string data = GetData();
				Log.Debug("Got data: '{0}'.", data);
				result = MeasurementInfo.Parse(data);
			}
			catch (Exception ex) {
				Log.Error (appName, ex.ToString ());
			}
			return result;
		}
		
		string GetData()
		{
			string result = "";
			HttpWebRequest request = (HttpWebRequest)WebRequest.Create ("http://johnson-baby.info/wview/wxrss.xml");
			using (HttpWebResponse response = (HttpWebResponse)request.GetResponse ())
			{
				if (response.StatusCode == HttpStatusCode.OK)
				{
					using (var stream = response.GetResponseStream())
					{
						XDocument xml = XDocument.Load(stream);

						result = xml.Root
							.Element("channel")
								.Element("item")
								.Element("{http://purl.org/rss/1.0/modules/content/}encoded")
								.Value;
					}
				}
				else
					throw new InvalidOperationException("Unexpected HTTP code: " + response.StatusCode);
			}
			
			return result;
		}
	}
}
