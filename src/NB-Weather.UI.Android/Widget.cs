using System;
using System.Linq;
using System.Net;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Linq;
using Android.App;
using Android.Appwidget;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using Android.Util;

namespace NBWeather.UI.Android
{
	[BroadcastReceiver (Label = "@string/widget_name", Icon = "@drawable/icon")]
	[IntentFilter (new string [] { "android.appwidget.action.APPWIDGET_UPDATE" })]
	[MetaData ("android.appwidget.provider", Resource = "@xml/widget")]
	public class Widget : AppWidgetProvider
	{
		public override void OnUpdate (Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds)
		{
			// To prevent any ANR timeouts, we perform the update in a service
			context.StartService (new Intent (context, typeof (UpdateService)));
		}

		public override void OnDisabled (Context context)
		{
			base.OnDisabled (context);
		}

	}
}
