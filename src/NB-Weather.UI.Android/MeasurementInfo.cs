using System;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Globalization;

namespace NBWeather.UI.Android
{
	public class MeasurementInfo
	{
		const string sample = "\n        <p>\n        Temp: -0.6 C<br>\n        Wind Chill: -2.9 C<br>\n        Heat Index: -0.6 C<br>\n";
		//<p> Temp: 0.3 C<br> Wind Chill: -3.3 C<br> Heat Index: 0.3 C<br> Humidity: 84 %<br> Dewpoint: -2.1 C<br> Barometer: 1005.3 mb<br> Wind: SSE at 3.1 m/s<br> Rain Today: 0.52 mm<br> Rain Rate: 0.00 mm<br> </p>
		
		
		static readonly Regex regexTemp = new Regex(@"(?<=Temp:\s+)\-?\d+\.\d+", RegexOptions.Compiled);
		static readonly Regex regexWindChill = new Regex(@"(?<=Wind\s+Chill:\s+)\-?\d+\.\d+", RegexOptions.Compiled);
		static readonly Regex regexHumidity = new Regex(@"(?<=Humidity:\s+)\d{1,3}", RegexOptions.Compiled);
		static readonly Regex regexBarometer = new Regex(@"(?<=Barometer:\s+)\-?\d+", RegexOptions.Compiled);
		static readonly Regex regexWind = new Regex(@"Wind:\s+(?<dir>[NWSE]+)\s+at\s+(?<speed>\d+\.\d+)\s+m/s", RegexOptions.Compiled);
		static readonly Regex regexRainToday = new Regex(@"(?<=Rain\s+Today:\s+)\d+\.\d+", RegexOptions.Compiled);
		static readonly Regex regexRainRate = new Regex(@"(?<=Rain\s+Rate:\s+)\d+\.\d+", RegexOptions.Compiled);

		public decimal Temp;
		public decimal WindChill;
		public byte Humidity;
		public int Barometer;
		public string WindDirection;
		public decimal WindSpeed;
		public decimal RainToday;
		public decimal RainRate;

		public static MeasurementInfo Parse(string input)
		{
			var result = new MeasurementInfo();
			
			result.Temp = TryParse(regexTemp.Match(input), Convert.ToDecimal);
			result.WindChill = TryParse(regexWindChill.Match(input), Convert.ToDecimal);
			result.Humidity = TryParse(regexHumidity.Match(input), Convert.ToByte);
			result.Barometer = TryParse(regexBarometer.Match(input), Convert.ToInt32);

			Match match = regexWind.Match(input);
			if (match.Success)
			{
				result.WindDirection = TryParse(match.Groups["dir"].Value, Convert.ToString);
				result.WindSpeed = TryParse(match.Groups["speed"].Value, Convert.ToDecimal);
			}

			result.RainToday = TryParse(regexRainToday.Match(input), Convert.ToDecimal);
			result.RainRate = TryParse(regexRainRate.Match(input), Convert.ToDecimal);

			return result;
		}

		static T TryParse<T>(Match match, Func<string, IFormatProvider, T> converter)
		{
			T result = match.Success
				? TryParse(match.Value, converter)
				: default(T);
			return result;
		}

		static T TryParse<T>(string input, Func<string, IFormatProvider, T> converter)
		{
			try
			{
				T result = converter(input, CultureInfo.InvariantCulture);
				return result;
			}
			catch (Exception ex)
			{
				Debug.WriteLine("Error parsing '{0}': {1}", input, ex);
				return default(T);
			}
		}
	}
}

